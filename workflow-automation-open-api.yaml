openapi: 3.0.3
info:
  title: Picsart Workflow Automation API
  description: Picsart Workflow Automation API document provides detailed description about how to interact with Workflow Automation API
  version: 0.1.0
  contact:
    email: info@picsart.io
tags:
  - name: Workflow
    description: Everything about workflow
  - name: Workflow Execution
    description: Everything about Workflow execution
servers:
  - url: 'http://localhost:8080'
    description: Local
paths:
  /workflows:
    post:
      summary: Get all workflows
      tags:
        - Workflow
      security:
        - bearerAuth: []
      parameters:
        - $ref: '#/components/parameters/ContentType'
      requestBody:
        $ref: '#/components/requestBodies/CreateWorkflow'
      responses:
        '201':
          description: A JSON object of workflow
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Workflow'
              examples:
                Workflow:
                  $ref: '#/components/examples/WorkflowExample'
                WorkflowInFailedStatus:
                  $ref: '#/components/examples/WorkflowInFailedStatusExample'
        '400':
          $ref: '#/components/responses/BadRequest'
        '401':
          $ref: '#/components/responses/UnauthorizedError'
        '422':
          description: Unprocessable Entity (WebDAV)
      description: Create workflow
      operationId: post-workflows
    get:
      summary: Add user workflows
      tags:
        - Workflow
      security:
        - bearerAuth: []
      parameters:
        - $ref: '#/components/parameters/ContentType'
      responses:
        '200':
          description: A list of user JSON object workflow
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Workflows'
              examples:
                Workflows:
                  $ref: '#/components/examples/WorkflowExamples'
        '401':
          $ref: '#/components/responses/UnauthorizedError'
      description: Get workflows
      operationId: get-workflows
    parameters: []
  '/workflows/{workflowId}':
    get:
      summary: Get the workflow
      tags:
        - Workflow
      security:
        - bearerAuth: []
      parameters:
        - $ref: '#/components/parameters/ContentType'
        - $ref: '#/components/parameters/InPathWorkflowId'
      responses:
        '200':
          description: A JSON object of logged in user
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Workflow'
              examples:
                inStartedStatus:
                  $ref: '#/components/examples/WorkflowExample'
                WorkflowInFailedStatusExample:
                  $ref: '#/components/examples/WorkflowInFailedStatusExample'
        '401':
          $ref: '#/components/responses/UnauthorizedError'
        '404':
          $ref: '#/components/responses/NotFound'
      description: Get workflow
      operationId: get-workflow
    delete:
      summary: Delete the workflow
      tags:
        - Workflow
      security:
        - bearerAuth: []
      parameters:
        - $ref: '#/components/parameters/ContentType'
        - $ref: '#/components/parameters/InPathWorkflowId'
      responses:
        '204':
          description: The resource was deleted successfully.
        '401':
          $ref: '#/components/responses/UnauthorizedError'
        '404':
          $ref: '#/components/responses/NotFound'
      description: Delete workflow
      operationId: delete-workflow
    parameters:
      - $ref: '#/components/parameters/InPathWorkflowId'
  '/workflows/{workflowId}/executions':
    post:
      summary: Start the workflow execution
      tags:
        - Workflow Execution
      security:
        - bearerAuth: []
      parameters:
        - $ref: '#/components/parameters/ContentType'
        - $ref: '#/components/parameters/InPathWorkflowId'
      requestBody:
        $ref: '#/components/requestBodies/StartWorkflowExecution'
      responses:
        '201':
          description: A JSON object of workflow excution
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/WorkflowExecution'
              examples:
                WorklowExecution:
                  $ref: '#/components/examples/WorkflowExecutionExample'
        '400':
          $ref: '#/components/responses/BadRequest'
        '401':
          $ref: '#/components/responses/UnauthorizedError'
      operationId: post-workflow-execution
      description: Start workflow execution
    get:
      summary: Get the workflow executions
      tags:
        - Workflow Execution
      security:
        - bearerAuth: []
      parameters:
        - $ref: '#/components/parameters/ContentType'
        - $ref: '#/components/parameters/InPathWorkflowId'
      responses:
        '200':
          description: A JSON object of user workflow executions
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/WorkflowExecution'
              examples:
                WorklowExecution:
                  $ref: '#/components/examples/WorkflowExecutionExample'
        '401':
          $ref: '#/components/responses/UnauthorizedError'
      description: Get workflows executions
      operationId: get-workflow-executions
    parameters:
      - $ref: '#/components/parameters/InPathWorkflowId'
  '/workflows/{workflowId}/executions/{executionId}':
    get:
      summary: Get the workflow execution
      tags:
        - Workflow Execution
      security:
        - bearerAuth: []
      parameters:
        - $ref: '#/components/parameters/ContentType'
        - $ref: '#/components/parameters/InPathWorkflowId'
        - $ref: '#/components/parameters/InPathWorkflowExecutionId'
      responses:
        '200':
          description: A JSON object of user workflow executions
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/WorkflowExecutions'
              examples:
                WorklowExecution:
                  $ref: '#/components/examples/WorkflowExecutionExamples'
        '401':
          $ref: '#/components/responses/UnauthorizedError'
      description: Get workflow execution
      operationId: get-workflow-execution
    parameters:
      - required: true
        name: workflowId
        in: path
        schema:
          $ref: '#/components/schemas/WorkflowId'
      - required: true
        name: executionId
        in: path
        schema:
          $ref: '#/components/schemas/ExecutionId'
components:
  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT
  parameters:
    ContentType:
      name: Content-Type
      in: header
      required: true
      schema:
        type: string
        example: application/json
      description: Content-type
    InPathWorkflowId:
      required: true
      name: workflowId
      in: path
      schema:
        $ref: '#/components/schemas/WorkflowId'
    InPathWorkflowExecutionId:
      required: true
      name: executionId
      in: path
      schema:
        $ref: '#/components/schemas/ExecutionId'
  schemas:
    Workflow:
      type: object
      x-examples:
        example-1:
          id: a89d9a21-2352-4b14-b887-ef850a0fedb3
          name: S3 to google drive processing
          template:
            version: '2022-01-15'
            description: S3 to google drive processing
            meta_data:
              - key: external-id
                value: BFA9325885541F435C442950A78C9C92
            parameters:
              - key: unique-id
                value: BFA9325885541F435C442950A78C9C92
            sources:
              - id: 9ce2a747-c228-464f-b451-e3bfb440a68a
                name: google_drive
                tags:
                  - source
            replay:
              id: 497f6eca-6276-4993-bfeb-53cbbbba6f08
            targets:
              - id: 9ce2a747-c228-464f-b451-e3bfb440a68a
                name: google_drive
                tags:
                  - source
          status: in_progress
          status_details:
            failed:
              - integration: google_drive
                error: 'Authentification issuee with google drive, .....'
          notifications:
            - id: 9ce2a747-c228-464f-b451-e3bfb440a68a
              name: google_drive
              tags:
                - source
          events:
            - ALL
          created_at: '2021-10-14T10:05:00+00:00'
      properties:
        id:
          $ref: '#/components/schemas/WorkflowId'
        name:
          $ref: '#/components/schemas/WorkflowName'
        template:
          $ref: '#/components/schemas/WorkflowTemplate'
        status:
          $ref: '#/components/schemas/WorkflowStatus'
        status_details:
          type: object
          properties:
            failed:
              type: array
              description: 'The failed property is null, if status is not failed'
              nullable: true
              items:
                type: object
                properties:
                  integration:
                    type: string
                    enum:
                      - google_drive
                      - aws_s3
                    description: The integration which is faling to connected
                  error:
                    type: string
                    description: The reason of fail
                    example: 'Authentification issuee with google drive, .....'
        notifications:
          type: array
          items:
            $ref: '#/components/schemas/Integration'
        events:
          $ref: '#/components/schemas/Events'
        created_at:
          $ref: '#/components/schemas/Date'
    Workflows:
      type: array
      items:
        $ref: '#/components/schemas/Workflow'
    WorkflowExecution:
      type: object
      x-examples:
        example-1:
          id: 2f1f7c08-e88a-44f5-8c80-cf59b186018f
          state: validating
          state_details:
            failed:
              - error: string
            completed:
              processed_images_count: 0
              execution_time: 0
          template:
            version: '2022-01-15'
            description: S3 to google drive processing
            meta_data:
              - key: external-id
                value: BFA9325885541F435C442950A78C9C92
            parameters:
              - key: unique-id
                value: BFA9325885541F435C442950A78C9C92
            sources:
              - id: 9ce2a747-c228-464f-b451-e3bfb440a68a
                name: google_drive
                tags:
                  - source
            replay:
              id: 497f6eca-6276-4993-bfeb-53cbbbba6f08
            targets:
              - id: 9ce2a747-c228-464f-b451-e3bfb440a68a
                name: google_drive
                tags:
                  - source
          events:
            - type: ALL
              details: string
          created_at: '2021-10-14T10:05:00+00:00'
      properties:
        id:
          type: string
          format: uuid
          example: 2f1f7c08-e88a-44f5-8c80-cf59b186018f
          description: Workflow execution identitier
        state:
          type: string
          enum:
            - validating
            - processing
            - failed
            - completed
          description: |
            *  `validating`: Validating posibility of execution.
            *  `processing`: In progress state.
            *  `failed`: At some point execution failed
            *  `completed`: Execustion successed
        state_details:
          type: object
          properties:
            failed:
              type: array
              description: 'The failed property is null, if status is not failed'
              nullable: true
              items:
                type: object
                properties:
                  error:
                    type: string
                    description: The reason of fail
            completed:
              type: object
              nullable: true
              properties:
                processed_images_count:
                  type: integer
                  description: How many images processed during this execution
                execution_time:
                  type: integer
                  description: The execution time in seconds
        template:
          $ref: '#/components/schemas/WorkflowTemplate'
        events:
          type: array
          description: The subscribed events
          items:
            type: object
            properties:
              type:
                $ref: '#/components/schemas/Event'
              details:
                type: string
                nullable: true
        created_at:
          $ref: '#/components/schemas/Date'
    WorkflowExecutions:
      type: array
      items:
        $ref: '#/components/schemas/WorkflowExecution'
    Event:
      type: string
      enum:
        - ALL
        - DATA_FETCHING_FAILED
        - DATA_FETCHING_SUCCEEDED
        - EXECUTION_FAILED
        - EXECUTION_SUCCEEDED
        - DATA_PUBLISH_FAILED
        - DATA_PUBLISH_SUCCEEDED
      x-examples:
        example-1: ALL
    Events:
      type: array
      items:
        $ref: '#/components/schemas/Event'
    WorkflowId:
      type: string
      format: uuid
      description: Identifier of workflow
    ExecutionId:
      type: string
      format: uuid
      description: Identifier of workflow execution
    WorkflowName:
      type: string
      minLength: 1
      maxLength: 200
      description: The name of workflow
      example: S3 to google drive processing
    WorkflowTemplate:
      type: object
      x-examples:
        example-1:
          version: '2022-01-15'
          description: S3 to google drive processing
          meta_data:
            - key: external-id
              value: BFA9325885541F435C442950A78C9C92
          parameters:
            - key: unique-id
              value: BFA9325885541F435C442950A78C9C92
          sources:
            - id: 9ce2a747-c228-464f-b451-e3bfb440a68a
              name: google_drive
              tags:
                - source
          solutions:
            - service: removeBackground
          targets:
            - id: 9ce2a747-c228-464f-b451-e3bfb440a68a
              name: google_drive
              tags:
                - source
      description: ''
      properties:
        version:
          type: string
          enum:
            - '2022-01-15'
          description: The template version date
          default: '2022-01-15'
        description:
          type: string
          minLength: 1
          maxLength: 500
          description: The detaild ecplonayion/description of the workflow
          example: S3 to google drive processing
          nullable: true
        meta_data:
          type: array
          description: 'Some useful properties, that can be used later'
          nullable: true
          items:
            type: object
            properties:
              key:
                type: string
                minLength: 1
                maxLength: 200
                example: external-id
              value:
                type: string
                minLength: 1
                maxLength: 200
                example: BFA9325885541F435C442950A78C9C92
        parameters:
          type: array
          description: 'Set of parameters, which can be used in any stage of the process'
          items:
            type: object
            properties:
              key:
                type: string
                minLength: 1
                maxLength: 200
                example: unique-id
              value:
                type: string
                minLength: 1
                maxLength: 200
                example: BFA9325885541F435C442950A78C9C92
        sources:
          type: array
          description: 'list of integration sources, which must be served as an image sources'
          items:
            $ref: '#/components/schemas/Integration'
        replay:
          $ref: '#/components/schemas/Replay'
        targets:
          type: array
          description: 'list of integration targets, which must be served as an output target'
          items:
            $ref: '#/components/schemas/Integration'
      required:
        - sources
        - replay
        - targets
    WorkflowStatus:
      type: string
      enum:
        - in_progress
        - ready
        - failed
        - deleted
      description: |
        *  `in_progress`: In validation stage of integrations and connections.
        *  `ready`: Ready to be used, the workflow can be executed.
        *  `failed`: Some parts of integrations or connections, could be insdie sources, (targets or notifications) are not authtificated.
        *  `deleted`: Customer has deleted the workflow.
    Integration:
      type: object
      properties:
        id:
          type: string
          format: uuid
          description: 'The integration id, whcih will be used to fetch all nussasary data from the source'
          example: 9ce2a747-c228-464f-b451-e3bfb440a68a
        name:
          type: string
          enum:
            - google_drive
            - aws_s3
            - dropbox
            - aws_sns
          description: The name of integration provider
          default: google_drive
        tags:
          type: array
          items:
            type: string
            enum:
              - source
              - target
              - notification
    Date:
      type: string
      format: date-time
      description: ISO 8601
      example: '2021-10-14T10:05:00+00:00'
    Replay:
      title: Replay
      type: object
      x-examples:
        example-1:
          id: b7d4377e-7d2f-4952-9f18-3f10ac6ae4d4
      properties:
        id:
          type: string
          format: uuid
      description: ''
  requestBodies:
    CreateWorkflow:
      required: true
      content:
        application/json:
          schema:
            type: object
            properties:
              name:
                $ref: '#/components/schemas/WorkflowName'
              template:
                type: object
                properties:
                  version:
                    type: string
                    nullable: true
                    enum:
                      - '2022-01-15'
                    description: The template version date
                    default: '2022-01-15'
                  description:
                    type: string
                    nullable: true
                    minLength: 1
                    maxLength: 500
                    description: The detaild ecplonayion/description of the workflow
                    example: S3 to google drive processing
                  meta_data:
                    type: array
                    description: 'Some useful properties, that can be used later'
                    items:
                      type: object
                      properties:
                        key:
                          type: string
                          minLength: 1
                          maxLength: 200
                          example: external-id
                        value:
                          type: string
                          minLength: 1
                          maxLength: 200
                          example: BFA9325885541F435C442950A78C9C92
                  parameters:
                    type: array
                    description: 'Set of parameters, which can be used in any stage of the process'
                    items:
                      type: object
                      properties:
                        key:
                          type: string
                          minLength: 1
                          maxLength: 200
                          example: unique-id
                        value:
                          type: string
                          minLength: 1
                          maxLength: 200
                          example: BFA9325885541F435C442950A78C9C92
                  sources:
                    type: array
                    description: 'list of integration sources, which must be served as an image sources'
                    items:
                      $ref: '#/components/schemas/Integration'
                  solutions:
                    type: array
                    description: list of solutions supported by Picsart API program
                    items:
                      properties:
                        service:
                          type: string
                          enum:
                            - removeBackground
                            - upscale
                            - styleTransfer
                            - filters
                            - adjust
                          description: |
                            *  `removeBackground`: Nice words about removeBackground service.
                            *  `upscale`: Nice words about upscale service.
                            *  `styleTransfer`: Nice words about styleTransfer service.
                            *  `filters`: Nice words about filters service.
                            *  `adjust`: Nice words about adjust service.
                  targets:
                    type: array
                    description: 'list of integration targets, which must be served as an output target'
                    items:
                      $ref: '#/components/schemas/Integration'
              notifications:
                type: array
                description: list of notiifcation integration
                items:
                  $ref: '#/components/schemas/Integration'
              events:
                type: array
                items:
                  $ref: '#/components/schemas/Event'
    StartWorkflowExecution:
      required: true
      content:
        application/json:
          schema:
            type: object
            properties:
              template:
                type: object
                properties:
                  meta_data:
                    type: array
                    description: 'Some useful properties, that can be used later'
                    items:
                      type: object
                      properties:
                        key:
                          type: string
                          minLength: 1
                          maxLength: 200
                          example: external-id
                        value:
                          type: string
                          minLength: 1
                          maxLength: 200
                          example: BFA9325885541F435C442950A78C9C92
                  parameters:
                    type: array
                    description: 'Set of parameters, which can be used in any stage of the process'
                    items:
                      type: object
                      properties:
                        key:
                          type: string
                          minLength: 1
                          maxLength: 200
                          example: unique-id
                        value:
                          type: string
                          minLength: 1
                          maxLength: 200
                          example: BFA9325885541F435C442950A78C9C92
  responses:
    BadRequest:
      description: Bad Request.
      content:
        application/json:
          schema:
            type: object
            properties:
              message:
                type: array
                items:
                  type: string
                  example: Validation failed
    UnauthorizedError:
      description: Authentication information is missing or invalid
      content:
        application/json:
          schema:
            type: object
            properties:
              message:
                type: string
                example: Unauthorized
    NotFound:
      description: Resource was not found.
      content:
        application/json:
          schema:
            type: object
            properties:
              message:
                type: string
                example: Resource not found by id 92262c27-a20b-4814-8717-53f25b69a4d8
  examples:
    WorkflowExample:
      value:
        name: S3 to google drive processing
        status: ready
        template:
          version: '2022-01-15'
          description: S3 to google drive processing
          meta_data:
            - key: external-id
              value: BFA9325885541F435C442950A78C9C92
          parameters:
            - key: unique-id
              value: BFA9325885541F435C442950A78C9C92
          sources:
            - id: 9ce2a747-c228-464f-b451-e3bfb440a68a
              name: google_drive
              tags:
                - source
          replay:
            id: 4e42257a-c4c1-4621-a140-86f8b016e41a
          targets:
            - id: 78952504-5ae7-48b5-b983-825ed6a27fd1
              name: google_drive
              tags:
                - target
        notifications:
          - id: 4dbf7b4d-9b86-49e3-93e1-7673f5c3d783
            name: aws_s3
            tags:
              - notification
        events:
          - ALL
    WorkflowExamples:
      value:
        - name: S3 to google drive processing
          status: ready
          template:
            version: '2022-01-15'
            description: S3 to google drive processing
            meta_data:
              - key: external-id
                value: BFA9325885541F435C442950A78C9C92
            parameters:
              - key: unique-id
                value: BFA9325885541F435C442950A78C9C92
            sources:
              - id: 9ce2a747-c228-464f-b451-e3bfb440a68a
                name: google_drive
                tags:
                  - source
            replay:
              id: 171854d8-5b10-4951-83b0-1db5f8483301
            targets:
              - id: 78952504-5ae7-48b5-b983-825ed6a27fd1
                name: google_drive
                tags:
                  - target
          notifications:
            - id: 4dbf7b4d-9b86-49e3-93e1-7673f5c3d783
              name: aws_s3
              tags:
                - notification
    WorkflowInFailedStatusExample:
      value:
        name: S3 to google drive processing
        status: failed
        status_details:
          failed:
            reason: 'Some parts of integrations or connections, could be insdie sources, (targets or notifications) are not authtificated.'
        template:
          version: '2022-01-15'
          description: S3 to google drive processing
          meta_data:
            - key: external-id
              value: BFA9325885541F435C442950A78C9C92
          parameters:
            - key: unique-id
              value: BFA9325885541F435C442950A78C9C92
          sources:
            - id: 9ce2a747-c228-464f-b451-e3bfb440a68a
              name: google_drive
              tags:
                - source
          replay:
            id: 6804e407-e91b-4a45-b9a5-7341bd42d2e1
          targets:
            - id: 78952504-5ae7-48b5-b983-825ed6a27fd1
              name: google_drive
              tags:
                - target
        notifications:
          - id: 4dbf7b4d-9b86-49e3-93e1-7673f5c3d783
            name: aws_s3
            tags:
              - notification
        events: null
    WorkflowExecutionExample:
      value:
        id: 2f1f7c08-e88a-44f5-8c80-cf59b186018f
        state: validating
        state_details: {}
        template:
          version: '2022-01-15'
          description: S3 to google drive processing
          meta_data:
            - key: external-id
              value: BFA9325885541F435C442950A78C9C92
          parameters:
            - key: unique-id
              value: BFA9325885541F435C442950A78C9C92
          sources:
            - id: 9ce2a747-c228-464f-b451-e3bfb440a68a
              name: google_drive
              tags:
                - source
          replay:
            id: b3bec418-cd30-4049-beec-21be5bd93e98
          targets:
            - id: 78952504-5ae7-48b5-b983-825ed6a27fd1
              name: google_drive
              tags:
                - target
        events:
          - type: DATA_FETCHING_FAILED
            details: Some description about fail reason
    WorkflowExecutionExamples:
      value:
        - id: 2f1f7c08-e88a-44f5-8c80-cf59b186018f
          state: validating
          state_details: {}
          template:
            version: '2022-01-15'
            description: S3 to google drive processing
            meta_data:
              - key: external-id
                value: BFA9325885541F435C442950A78C9C92
            parameters:
              - key: unique-id
                value: BFA9325885541F435C442950A78C9C92
            sources:
              - id: 9ce2a747-c228-464f-b451-e3bfb440a68a
                name: google_drive
                tags:
                  - source
            replay:
              id: 57edfadf-213e-469d-82f8-6d578060c218
            targets:
              - id: 78952504-5ae7-48b5-b983-825ed6a27fd1
                name: google_drive
                tags:
                  - target
          events:
            - type: EXECUTION_SUCCEEDED
              details: Fetching started from google drive
            - type: DATA_PUBLISH_SUCCEEDED
              details: Remove background services started to process images
